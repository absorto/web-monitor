# Perl Web Monitor

This is a basic script to monitor server uptime using HTTPS Requests. It can check website uptime and record the responses.
All you need to do is set this script in another server and call it from a crontab at a certain interval. redirecting STDOUT to a file.
The information logged is minimal, it registers when the request was made, how long it took to retrieve the document and the http response code it received.
The request timeout default is set to 10 seconds

## Basic usage:

- First parameter is the HTTP URL that is going to be tested

## Example:

```bash
$ web-monitor/web-monitor.pl https://thesite.you.wanna.monitor your_log_file
```

