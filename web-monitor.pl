#!/usr/bin/perl
# Basic script to monitor server uptime using HTTPS Requests
# It Logs the time the request is made, the time it takes to retrieve the doc and the http response it got
# The request timeout default is set to 10 seconds
# Basic usage:
# - First parameter is the HTTP URL that is going to be tested

use Time::localtime;
use LWP::UserAgent;
use Time::HiRes qw(time);

my $resp_time = 0;

# Log start time of request
my $start = time;
$date = qx( date );
chomp $date;

# Start the request, waiting for no more than 10 sec and record response
my $url = $ARGV[0];
my $ua = LWP::UserAgent->new;
$ua->timeout(10);

my $req = HTTP::Request->new(GET => $url);
my $resp = $ua->simple_request($req);

# Record the time of response
$resp_time = time - $start;
print "${date} GET '${url}' " . $resp->code . " " .  $resp->message  . " " . int($resp_time*1000) . "ms\n";
